// IMPORT: COMPONENTS
// import Banner from '../components/Banner.js';
// import Highlights from '../components/Highlights';


export default function Home(){

	// DATA TO BE DISPLAYED IN HOME PAGE BANNER
	const data = {
		title: "The Pet Curiosity!",
		subtitle: "A strong desire to learn something.",
		content: "You get cool shirts!, Dogs get warm shelter.",
		destination: "/collections",
		label: "Collections"
	}

	// PASS DATA TO BANNER PROP AND DISPLAY ALONG WITH HIGHLIGHTS
	return(
		<>
			{/*<Banner bannerProp={data}/>*/}
			{/*<Highlights/>*/}
		</>
	)
}